## 启动流程

> 打开app    ---->    请求接口 ----->  根据 status 判断 0: 展示h5, 1:展示本地马甲

> 具体看 [Api文档 api.md](https://gitlab.com/JC_/simplepackage/-/blob/master/api.md)


![](https://gitee.com/google_project_team/googlevestrequire/raw/master/src/main.png)  


## 需要找对接人获取的信息

 1、Facebook参数  
 2、Branch参数  

## 给对接人提供的信息文件
1、使用keytool获取签名文件 -MD5 - SHA1- SHA256 

```
keytool -list -v -keystore xxx.jks
```

2、Branch schema  

![](https://gitee.com/google_project_team/googlevestrequire/raw/master/src/branch_scheme_info.png)  

3、Application id  

![](https://gitee.com/google_project_team/googlevestrequire/raw/master/src/application_id.png)


## 测试链接说明  

![](https://gitee.com/google_project_team/googlevestrequire/raw/master/src/test_flow.jpeg)  


## 接入的三方
按照下面三方文档，接入 Branch 与 Facebook SDK。
<br>
[Branch 集成参考链接](https://help.branch.io/developers-hub/docs/android-basic-integration#section-configure-app)
<br>
[Facebook Android Sdk 集成参考链接 ](https://developers.facebook.com/docs/android/getting-started/) 按照文档接入 application Id 即可

## 一、 启动流程
![](https://gitee.com/google_project_team/googlevestrequire/raw/master/src/open.png)  
![](https://gitee.com/google_project_team/googlevestrequire/raw/master/src/main.png) 

> titlebar的文案从webview中获取 `实现WebClient` 在onPageFinished方法中 webView.getTitle()  

## 二、SDK版本等要求
* minSdkVersion 21  targetSdkVersion 30
* 必须支持64位  
* 应用支持http协议 cleartextTrafficPermitted="true"
* webview可以选取文件`WebChromeClient onShowFileChooser()`
* 一般情况下，webview中系统回回退键，webview可以后退的时候就后退，无法后退了在finish()
* 键盘输入适配(遮挡输入框的问题)

## 三、需要给对接人提供的信息文件
1、使用keytool获取签名文件-MD5 - SHA1- SHA256 

```
keytool -list -v -keystore xxx.jks
```

2、Branch schema  

![](https://gitee.com/google_project_team/googlevestrequire/raw/master/src/branch_scheme_info.png)  

3、Application id  

![](https://gitee.com/google_project_team/googlevestrequire/raw/master/src/application_id.png)

## 四、需要找对接人获取的信息
1、开关的 host与code    
3、Branch参数  
4、Firebase账号 

## 五、WebView设置
``` xml
<string name="android_web_agent">ANDROID_AGENT_NATIVE/2.0&#8194;%1$s</string>
```


``` kotlin 
 webView.apply {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        WebView.setWebContentsDebuggingEnabled(false)
    }
    clearHistory()
    isDrawingCacheEnabled = true
    scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
    addJavascriptInterface(AppJs(this@WebActivity), "AppJs")
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        setLayerType(View.LAYER_TYPE_HARDWARE, null)
    } else {
        setLayerType(View.LAYER_TYPE_SOFTWARE, null)
    }
   //需要实现WebViewClient
   webViewClient =  

    
    ...
    ...
    ...
    
    setOnLongClickListener {
        val result = webView.hitTestResult
        if (result != null) {
            val type = result.type
            if (type == WebView.HitTestResult.IMAGE_TYPE) {
                showSaveImageDialog(result)
            }
        }
        false
    }
    setDownloadListener { url, _, _, _, _ ->
        val uri = Uri.parse(url)
        val intent = Intent(Intent.ACTION_VIEW, uri)
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    } 
    settings.apply {
        var userAgentString = this.userAgentString
        userAgentString = getString(R.string.android_web_agent, userAgentString)
        this.userAgentString = userAgentString  
        
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
        }
        setRenderPriority(WebSettings.RenderPriority.NORMAL)
            
        javaScriptEnabled = true
        javaScriptCanOpenWindowsAutomatically = true
        cacheMode = WebSettings.LOAD_DEFAULT
        databaseEnabled = true
        setAppCacheEnabled(true)
        setAppCachePath(externalCacheDir?.path)
        allowFileAccess = true
        domStorageEnabled = true
        useWideViewPort = true
        loadWithOverviewMode = true
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
        }
    }
}
```


## 六、AppJs给H5提供的交互方法  

> 类名可以随意，只要注册的interfaceName是`AppJs`即可  


```

const val GET_DEVICE_ID = "getDeviceId"
const val GET_GA_ID = "getGaId"
const val GET_GOOGLE_ID = "getGoogleId"
const val IS_CONTAINS_NAME = "isContainsName"
const val OPEN_BROWSER = "openBrowser"
const val OPEN_PURE_BROWSER = "openPureBrowser"
const val SHOW_TITLE_BAR = "showTitleBar"
const val TAKE_CHANNEL = "takeChannel"
const val BRANCH_EVENT = "branchEvent"

// 注册到web的方法
// addJavascriptInterface(xxx(), "AppJs")
class xxx {
    @JavascriptInterface
    fun isNewEdition(): String {
        return "true"
    }
    
     @JavascriptInterface
    override fun callMethod(data: String): String? {
        val dataObj = JSONObject(data)
        val methodName = dataObj.optString("name")
        val paraObj = dataObj.optJSONObject("parameter")
        val paraStr = dataObj.optString("parameter")
        when (methodName) {
            GET_DEVICE_ID -> {
                return getDeviceId()
            }
            SHOW_TITLE_BAR -> {
                showTitleBar(paraStr.toBoolean())
            }
            BRANCH_EVENT -> {
                //格式为：{"eventName":"content_click","parameters":"{}","alias":"content_click"}
                val brEvent = Gson().fromJson(paraStr, BrEvent::class.java)
                brEvent.apply {
                    if (eventName.isNotBlank()) {
                        if (alias.isBlank()) {
                            branchEvent(eventName, parameters)
                        } else {
                            branchEvent(eventName, parameters, alias)
                        }
                    }
                }
            }
            ···
            else -> {
            }
        }
        return null
    }
}
```
### 以下为需要实现的方法(最好单独写在其他类)

``` kotlin  
/**
 * 获取设备id
 * 必须保证有值 
 * 获取不到的时候生成一个UUID
 */
fun getDeviceId(): String {
    //TODO 
}

/**
 * 获取ANDROID_ID
 * public static final String ANDROID_ID
 */
fun getGoogleId(): String {
    //TODO 返回ANDROID_ID
}

/**
 * 集成branch包的时候已经带有Google Play Service核心jar包
 * 获取gpsadid 谷歌广告id
 * AdvertisingIdClient.getAdvertisingIdInfo() 异步方法
 */
fun getGaId(): String? {
    //TODO
}

/**
 * 获取应用渠道
 */
fun takeChannel(): String {
    return "google"
}

/**
 * 控制webview是否显示 TitleBar
 * （点击返回键webview 后退）
 * @param visible
 */
fun showTitleBar(visible: Boolean) {
    //TODO
}

fun branchEvent(eventName: String) {
    BranchEvent(eventName).logEvent(mContext)
}

fun branchEvent(eventName: String, parameters: String) {
    val branchEvent = BranchEvent(eventName)
    val obj = JsonObject().getAsJsonObject(parameters)
    if (obj != null) {
        for (entry in obj.entrySet()) {
            val value = entry.value
            branchEvent.addCustomDataProperty(
                entry.key,
                value.asString
            )
        }
    }
    branchEvent.logEvent(mContext)
}

fun branchEvent(eventName: String, parameters: String, alias: String) {
    val branchEvent = BranchEvent(eventName)
    val obj = JsonObject().getAsJsonObject(parameters)
    if (obj != null) {
        for (entry in obj.entrySet()) {
            val value = entry.value
            branchEvent.addCustomDataProperty(entry.key, value.asString)
        }
    }
    branchEvent.setCustomerEventAlias(alias).logEvent(mContext)
}


/**
 * 使用手机里面的浏览器打开 url
 *
 * @param url 打开 url
 */
fun openBrowser(url: String) {
    Log.e(TAG, "openBrowser url$url")
    if (mContext is WebActivity) {
        val uri = Uri.parse(url)
        val intent = Intent().apply {
            action = Intent.ACTION_VIEW
            data = uri
        }
        if (intent.resolveActivity(mContext.packageManager) != null) {
            mContext.startActivity(intent)
        }
    }
}

/**
 *  AppJs是否存在交互方法 告诉H5是否存在传入的对应方法
 *
 * @param name 方法名
 */
fun isContainsName(callbackMethod: String, name: String) {
   Log.v(TAG, "isContainsName:${callbackMethod};${name}")
   val has: Boolean = when (name) {
        FORBID_BACK_FOR_JS,GET_DEVICE_ID,GET_GA_ID,... -> {
            true
        }
        else -> {
            false
        }
    }
    if (context is WebActivity) {
        context.runOnUiThread {
            val webView = context.getWebView()
            val javaScript = "javascript:$callbackMethod('${has.toString()}')"
            webView.evaluateJavascript(javaScript, null)
        }
    }
}
    
/**
 * 打开一个基本配置的webview （不修改UA、可以缓存）
 * 打开新页面 
 * 加载webview的情况分类(判断依据：url、postData、html)
 *    |-------1、只有url：webView.loadUrl()
 *    |-------2、有url和postData：webView.postUrl()
 *    |-------3、有html webView.loadDataWithBaseURL()
 *
 * @param json 打开web传参 选填
 * {"title":"", 打开时显示的标题
 *  "url":"", 加载的地址
 *  "hasTitleBar":false, 是否显示标题栏
 *  "rewriteTitle":true, 是否通过加载的Web重写标题
 *  "stateBarTextColor":"black", 状态栏字体颜色 black|white
 *  "titleTextColor":"#FFFFFF", 标题字体颜色
 *  "titleColor":"#FFFFFF", 标题背景色
 *  "postData":"", webView post方法时需要传参
 *  "html":"", 加载htmlCode,
 *  "webBack":true, true:web回退(点击返回键webview可以回退就回退，无法回退的时候关闭该页面)|false(点击返回键关闭该页面) 直接关闭页面
 * }
 */
fun openPureBrowser(json: String) {
    Log.v(TAG, "openPureBrowser json:$json")
}

/**
 * 使用手机里面的浏览器打开 url
 * 注：如果是WhatsApp对话跳转，先判断本地是否安装了WhatsApp，
       如果已经安装直接跳转，没有安装再打开浏览器
 *
 * @param url 打开 url
 */
fun openBrowser(url: String) {
    if (mContext is WebActivity) {
        val uri = Uri.parse(url)
        val intent = Intent().apply {
            action = Intent.ACTION_VIEW
            data = uri
            //判断是否为WhatsApp、Telegram 跳转 且本地已安装
            if (url.contains("https://api.whatsapp.com/") && isAPPInstalled(mContext, "com.whatsapp")) {
                `package`="com.whatsapp"
            }
            if (url.contains("https://t.me/") && isAPPInstalled(mContext, "org.telegram.messenger")) {
                `package` = "org.telegram.messenger"
            }
        }
        if (intent.resolveActivity(mContext.packageManager) != null) {
            mContext.startActivity(intent)
        }
    }
}

fun isAPPInstalled(context: Context, packageName: String): Boolean {
    return try {
        context.applicationContext.packageManager.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES)
        true
    } catch (e: PackageManager.NameNotFoundException) {
        false
    }
}

```

